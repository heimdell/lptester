
data Pair a b = a := b deriving (Show)

main :: IO ()
main = do
    logs <- getContents

    let longEnouth = ((8 <=) . length) `filter` lines logs

    let terms      = dropWhile garbage longEnouth

    let countBit i = count terms $ ('1' ==) . (!! i)

    let bits       = map countBit [0..7]

    print ("bits" := bits, "count" := length terms)

garbage :: String -> Bool
garbage = not . all (`elem` "01")

count :: [a] -> (a -> Bool) -> Int
count = flip $ (length .) . filter
--count list property = length (property `filter` list)
