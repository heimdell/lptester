
CC_PREFIX  = 
CC         = g++
C_FLAGS    = -std=c++0x -Wall -ggdb -Wextra -pedantic -Werror
EXECUTABLE = bench
CHECKER    = CheckSteps
MAIN_FILE  = bench.c++
TEST_FILE  = test.steps

all: clrscr lptester

clrscr:
	# Clrscr

run: all
	./$(EXECUTABLE) $(TEST_FILE)

lptester: *.c++ *.h++
	$(CC_PREFIX)$(CC) $(C_FLAGS) $(MAIN_FILE) -o ./$(EXECUTABLE)

clean:
	-rm ./$(EXECUTABLE)
	-rm ./*.hi
	-rm ./*.o
	-rm ./$(CHECKER)
	-rm log

$(CHECKER): ./$(CHECKER).hs	

checker: ./$(CHECKER)
	ghc ./$(CHECKER).hs

test: checker all
	time -f "real\t%E\nuser\t%U\nsys\t%S\n" ./$(EXECUTABLE) -s $(TEST_FILE) 1> log 
	./$(CHECKER) < log
	rm log

dump: checker all
	./$(EXECUTABLE)	