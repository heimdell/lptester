
#include <string>
#include <iostream>

#include <queue>

#include "clock.h++"

#include "move.h++"

using namespace std;

/*
    Стандартный механизм вывода.
 */ 
struct Writer {
    void output(char c) {
        (cout << c).flush();
    }
};

/*
    Отладочный механизм вывода.
 */
struct BitWriter {
    void output(char c) {
        for (int i = 0; i < 8; i++) {
            cout << (char) ('0' | (c & 1));

            c /= 2;
        }
        cout << endl;
    }
};

template
  < int   CARRIER_
  , class Writer
  , class Pins
  >
struct VM {
    /*
        По теореме Котельникова.
     */
    enum { CARRIER = CARRIER_ / 2 };

    /*
        Тип-"операция".
     */
    typedef Move<CARRIER, Pins> Command;

    /*
        Каждый шаг состоит из двух действий - возможная установка бита и сброс 
         - для появления фронта и спада (поскольку усилитель перед мотором срабатывает по фронту).
     */
    enum class State { WRITE, CLEAR };

    queue<Command> program;
    mutex          program_block;

    Writer         writer;

    Command        nop = Command::StandStill;

    State          status;

    /*
        Добавление элементов программы.
     */
    template <class... Args>
    void push(double time, Args... args) {
        Lock lock(program_block);

        program.emplace(time, args...);
    }

    /*
        Добавление элементов программы оптом.
     */
    void push_vector(vector<Command> v) {
        Lock lock(program_block);

        for (auto x : v) program.push(x);
    }

    /*
        Переключение режима фронта/спада. 

        Вызывается на каждом шаге, дважды.
     */
    void toggle_status() {
        switch (status) {
            case State::WRITE: status = State::CLEAR;
            default:           status = State::WRITE;
        }
    }

    /*
        Выполнить 1 шаг, вывести его результат на станок.
     */
    void perform(Command &cmd) {
        auto c = cmd.next();

        writer.output(c);
    }

    /*
        Извлечение шага из очереди.
     */
    Command &next_command() {
        Command * opcode;

        locked(program_block, [this, &opcode] () {
            opcode = &program.front();
        });

        /*
            Отработавший шаг извлекается из очереди.
         */
        if (opcode->done()) {

            locked(program_block, [this] () {
                program.pop();
            });

        }

        return *opcode;
    }

    /*
        Главный цикл работы.
     */
    void start_loop() {

        /*
            g++ версии 4.8.2 имеет проблемы с захватом this.
         */
        auto *it = this;

        int i = 0;

        /*
            Описание шага работы.
         */
        auto step = [&i, it] () {
            bool no_work_to_do;

            locked(it->program_block, [it, &no_work_to_do] () {
                no_work_to_do = it->program.empty();
            });

            /*
                Возврат false завершает рабочий цикл.
             */
            if (no_work_to_do) { 
                return false; 
            }

            /*
                Создаём фронт/спад.
             */
            it->toggle_status();

            /*
                В зависимости от режима исполняем команду или выводим 0.
             */
            switch (it->status) {
                case State::WRITE: {
                    auto &command = it->next_command();
                    it->perform(command); 
                    break;
                }

                case State::CLEAR: 
                    it->perform(it->nop);

                default:;
            }

            return true;
        };

        schedule(step, CARRIER); //.join();
    }
};
