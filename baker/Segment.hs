
{-# LANGUAGE RecordWildCards #-}

module Segment where

import Control.Arrow

import Data.Array
import Data.List

data Segment carrier' = Segment 
  { carrier :: carrier'
  , from    :: Int
  , to      :: Int
  }

    deriving Show

seg `at` i = carrier seg ! i

i2d = fromIntegral

enlarge segment @ Segment {..} = segment { to = to + 1 }
inbound segment @ Segment {..} = to <= snd (bounds carrier)
next    segment @ Segment {..} = segment { from = to, to = to + 2}

ends Segment {..} = 
    ( (i2d from, carrier ! from)
    , (i2d to,   carrier ! to)
    )

--angle segment = -- (* 180) . (/ pi) .
--    atan $ (fy - sy) / (fx - sx)
  
--  where
--    ((sx, sy), (fx, fy)) = ends segment

--angleBoundsWidth dx dist = (* 180) . (/ pi) .
--    atan $ dx / dist

distance segment (x, y) =
    (sy - y) + (x - sx) * (fy - sy) / (fx - sx)

  where
    ((sx, sy), (fx, fy)) = ends segment

l2a list = array (0, length list - 1) $ zip [0..] list

seg = Segment (l2a [1,2,3,4,4,5,5,6,6,7,7,6,5,4,3,2,1]) 0 3

allSeg          = (and .) . mapSeg
mapSeg seg pred = for [from seg.. to seg] $ pred

for = flip map

distancesInbound seg = allSeg seg $ \i -> abs (distance seg (i2d i, seg `at` i)) < 0.7

swallowLine seg | not $ inbound $ enlarge seg = seg
swallowLine seg | distancesInbound seg = swallowLine $ enlarge seg
swallowLine seg = seg

resetBeyondStart segment @ Segment {..} = segment { to = 0 }

chessGroupBy' _    pred2 []    = []
chessGroupBy' _    pred2 [x]   = [[x]]
chessGroupBy' flag pred2 other = 
    let (gr, rest) = (map fst * map fst) $ break snd $ zipWith (\a b -> (a, a >= b ^^ flag)) other $ tail other

    in gr : chessGroupBy' (not flag) pred2 rest

breakToMonotonic = groupBy $ \l r -> l >= r

crawl = map ends . tail . takeWhile inbound . iterate (swallowLine . next) . \list -> Segment (l2a list) 0 0