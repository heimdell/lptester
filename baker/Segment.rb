
module Path

    LineSegment = Struct.new :carrier, :start, :finish, :epsilon do
        def inbound?
            finish < carrier.size
        end

        def expand
            self.finish += 1
        end

        def retract
            self.finish -= 1
        end

        def next
            LineSegment.new carrier, finish, finish + 2, epsilon
        end

        def distance x, y
            return 0 unless inbound?

            sx, sy = start,  carrier[start]
            fx, fy = finish, carrier[finish]

            dist = (sy - y) + (x - sx) * (fy - sy) / (fx - sx)
            dist.abs
        end

        def distances
            (start..finish).map do |x|
                distance(x, carrier[x])
            end
        end

        def valid?
            distances.all? do |y|
                y <= epsilon
            end
        end

        def swallow
            expand while inbound? and valid?
            retract
            self
        end
    end

    def self.start array, prec
        LineSegment.new array, 0, 0, prec
    end

    def self.chain array, prec
        segs = []
        seg  = start array, prec

        loop do
            seg = seg.next
            
            break unless seg.inbound?
            
            segs << seg.swallow
        end

        segs.map do |seg| 
            array[seg.start.. seg.finish]
        end
    end

    def self.scale kx, ky, fun
        lambda { |x| ky * fun.(x / kx) }
    end

    SIZE = 10000

    test = (1.. SIZE).map do |i|
        scaled_sin = self.scale 200, 100, Math.method(:sin)

        scaled_sin.(i + 0.0)
    end

    lines = chain test, 1.0

    puts SIZE / lines.size 
end