
--module Step where

import Data.List
import Control.Arrow

data Step
    = Shift Int
    | Func (Int -> Int)

data Timed a = Int `Steps` a

type Program = [Timed Step]

--breakToShifts :: Program -> Program
--breakToShifts = map $ \(dt `Steps` action) ->
--    case action of
--        Shift n -> Shift n
--        Func  f -> map Shift $ dt `interpolate` f

-- 0 = sy + (x - sx) * (fy - sy) / (fx - sx) - y
--
-- sy + x (dy / dx) - sx * (dy / dx) - y

distance1 (sx, sy) (fx, fy) (x, y) =
    sy + dydx * (x - sx) - y

  where
    dydx = (fy - sy) / (fx - sx)

doubles lim = [0.. lim]

distance' :: (Double, Double, Double) -> (Double, Double) -> Double
distance' (a, b, c) (x, y) = a * x + b * y + c

endsToCoefficiens :: (Double, Double) -> (Double, Double) -> (Double, Double, Double)
endsToCoefficiens (sx, sy) (fx, fy) =
    ((fy - sy) / (fx - sx), -1, sy)

distance = (distance' .) . endsToCoefficiens

step list =
    let crds = (map fromIntegral [0.. length list]) `zip` list in

    let mapper = distance1 (0, head list) (fromIntegral $ length list - 1, last list) in

    map mapper crds


operate 
    =   inits 
    >>> dropWhile ((2 >) . length) 
    >>> map        (step >>> map abs >>> maximum) 
    >>> break      (1 <) 
    >>> (length *** length)

chain :: [Double] -> [[Double]]
chain list =
    let (line, rest) = operate list in

    if rest == 0
    then [list]
    else take (line + 1) list : chain (drop line list)

main = do
    print $ length $ chain $ map ((* 100) . sin . (/ 200)) [1.. 10000]