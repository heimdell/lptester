
#include <tclap/CmdLine.h>

#include "VM.h++"

#include "program_reader.h++"

#include "function_library.h++"

/*
    Точка входа.
 */
int main(int argc, char * argv[]) {

    using namespace TCLAP;

    try {
        /*
            Настройка парсера командной строки.
         */
        CmdLine cmd("Bench is out.", ' ', "1.0");

        ValueArg<string> sourceArg   ("s", "source",        "file with movement instructions",       false, "what", "string");
        SwitchArg        onlyCheck   ("c", "only-check",    "stops right after checking program",    false);
        SwitchArg        testInterpol("t", "test-interpol", "debug test of interpolation mechanism", false);

        cmd.add(sourceArg);
        cmd.add(onlyCheck);
        cmd.add(testInterpol);

        cmd.parse(argc, argv);

        auto filename   = sourceArg.getValue();
        auto check_only = onlyCheck.getValue();

        /*
            Настройка параметров станка:
             - частота = 47,5 кГц
             - оси: x = биты 1-2, y = 3-4, z = отсутствует
             - пустой сигнал = 0
         */
        typedef VM
          < 47500
          , BitWriter
          , Pins
            < Ax<1, 2>
            , Ax<3, 4>
            , Ax<0, 0>
            , 0
            >
          >
        TestVM;

        /*
            Создание машины.
         */
        TestVM machine;

        /*
            Получение программы работы.
         */
        ProgramReader<TestVM> reader;

        machine.push_vector(reader.readFrom(filename));

        unless (check_only) {
            /*
                Запуск программы.
             */
            machine.start_loop();
        } else {
            cout << "Program seems to be valid." << endl;
        }

        cout << endl;
    }
    catch(runtime_error e) {
        cout << e.what() << endl;
    }
    catch (TCLAP::ArgException &e)
    {

    }

}
