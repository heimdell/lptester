
#ifndef BITS_HPP
#define BITS_HPP

constexpr int bit(int n) {
    return
          n == 0 ? 0
        : n == 1 ? 1
                 : 2 * bit(n - 1);
}

#endif
