
#ifndef CLOCK
#define CLOCK

#include <mutex>
#include <thread>
#include <chrono>

#include <iostream>
#include <sstream>

#include "macroses.h++"

using namespace std;
using namespace std::chrono;

void mark(string name) { cout << "@" << name << endl; }

/*
    Реализация "защёлки" над мьютексом.
 */
struct Lock {
    mutex &lock;

     Lock(mutex &lock) : lock(lock) { lock.lock(); }
    ~Lock()                         { lock.unlock(); }
};

template <class Action>
void locked(mutex &m, Action action) {
    Lock lock { m };

    return action();
}

/*
    Включает режим работы с неизменным пройденным расстоянием по осям.

    В случае, если программа обнаружит остановку своего выполнения, она постарается 
     наверстать упущенное вовремя.
 */
#define INVARIANT_PATH

/*
    Вывод отладочной информации в stdlog.
 */
// #define PERFORMING_CALIBRATIONS

template <class Action>
void schedule
  ( Action                action
  , float                 freq
  , float const           WARNING_AT = 50)
{
    // return thread([action, freq, WARNING_AT](){

    /*
        Единица работы - 1 сек.
     */
    int whole = 1000 * 1000;

    int quant = whole / freq;

    auto
        prev = high_resolution_clock::now(),
        #ifdef PERFORMING_CALIBRATIONS
        start = prev,
        #endif
        next = prev + microseconds(quant);

    long long int count = 0;

    loop {
        /*
            Дожидаемся начала следущего периода несущей частоты.
        */
        while (high_resolution_clock::now() < next) {
            this_thread::sleep_for(microseconds(quant / 2));
        }

        int i = 0;

        /*
            Обработка всех задержек.
         */
        while (high_resolution_clock::now() >= next) {
            prev = next,
            next = prev + microseconds(quant);

            #ifdef INVARIANT_PATH

                i++; // подсчёт

            #else

                i = 1; // постулирование

            #endif
        }

        /*
            Вывод предупреждающего сообщения при сильных задержках.
         */
        if (i > WARNING_AT) { clog << "\n! CANNOT KEEP UP FOR " << i * quant / (double) whole << " seconds" << endl; }

        while (i--) {
            if (!action()) {
                goto end;
            }

            /*
                Пауза для срабатывания движка.
             */
            this_thread::sleep_for(microseconds(quant));

            count++;

            #ifdef PERFORMING_CALIBRATIONS

                /*
                    Вывод отладочных данных.
                 */
                if (count % (long long int) (freq / 8)); else if (count) {
                    float real_freq  = 1000 * 1000 * 1000 * count / duration_cast<nanoseconds> (high_resolution_clock::now() - start).count();
                    float difference = (real_freq - freq) / freq * 100;

                    stringstream log;

                    log
                        << "\r# freq = " << real_freq  << " Hz "
                        << "\tn = "      << count
                        << "\ttime = "   <<
                            duration_cast<microseconds> (high_resolution_clock::now() - start)
                           .count() / (double) 1000000
                        << " s \tquant = "
                        << quant
                        << " ms"
                        << "\tdiff = "   << difference << "%"
                    ;

                    auto line = log.str();

                    (clog << "\r" << string(line.size() + 40, ' ') << line).flush();

                    if (difference >= WARNING_AT) {
                        clog << " -- WARNING, error > " << WARNING_AT << " %!" << endl;
                    }
                }

            #endif
        }
    }

end:
    clog << endl;
}

#endif
