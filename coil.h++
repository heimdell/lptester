
#ifndef COIL_HPP
#define COIL_HPP

#include <cmath>

#include "VM.h++"

inline int hypothenusis(int a, int b) {
    return sqrt(a * a + b * b);
}

template <int FREQ, class Writer, class Pins>
void coil(int w, int l0, int n, int dl) {
    typedef VM<FREQ, Writer, Pins> CoilMaker;

    CoilMaker coil;

    for (int i = 0; i < n; i++) {

        int l = l0 + i * dl;

        int path = hypothenusis(w, l);

        coil.push(path / (l / (double) l0), w, l);

        if (i++ >= n) break;

        l += dl;

        path = hypothenusis(w, l);

        coil.push(path / (l / (double) l0), -w, l);
    }

    coil.start_loop();
}

template <int FREQ, class Writer, class Pins>
void square_test() {
    // should make a square

    typedef VM<FREQ, Writer, Pins> Pod;

    Pod pod;

    pod.push(10, +11, +7, 0);
    pod.push(20, +11, -7, 0);
    pod.push(40, -11, +7, 0);
    pod.push(80, -11, -7, 0);

    pod.start_loop();
}

#endif
