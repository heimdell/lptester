
#ifndef DIALOG_HPP
#define DIALOG_HPP

#include <vector>
#include <sstream>

using namespace std;

#include "macroses.h++"

template <class T>
bool in(T item, vector<T> list) {
    for (auto i = list.begin(); i < list.end(); i++) {
        if (*i == item)
            return true;
    }
    return false;
}

bool isSpace(char c) {
    return in(c, { ' ', '\t' });
}

bool isLn(char c) {
    return c == '\n';
}

template <class Result>
Result read(string line) {
    stringstream str(line);

    Result result;

    str >> result;

    return result;
}

inline string glue() { return ""; }

template <class Arg, class... Args> 
inline string glue(Arg arg, Args... args)
{
    stringstream str;

    str << arg;
    str << glue(args...);

    return str.str();
}

template <class Line>
inline string unlines(Line line) { return glue(line); }

template <class Arg, class... Args> 
inline string unlines(Arg arg, Args... args)
{
    stringstream str;

    str << arg;
    str << endl;
    str << unlines(args...);

    return str.str();
}

#define tail(tokens) tokens.erase(tokens.begin())

vector<string> words(string input, function<bool(char)> isSpace = isSpace) {
    vector<string> accum;

    auto
        start  = input.begin(),
        end    = input.begin(),
        border = input.end();

    loop {
        for (start = end;  isSpace(*start) && start < border; start++);

        for (end = start; !isSpace(*end)   && end   < border; end  ++);

        if (start == end) break;

        accum.emplace_back(string(start, end));
    }

    return accum;
}

template <class T>
T head(vector<T> list) {
    return list.empty() ? T() : list[0];
}

#endif
