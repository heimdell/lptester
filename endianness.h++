
#ifndef ENDIANNESS_HPP
#define ENDIANNESS_HPP

#include <algorithm>
#include "sizes.h++"

using namespace std;

/*
    Библиотека для работы с endianness.
 */
template <int size> int swapBytes(int x);

static bool is_big_endian();

template <>
int swapBytes<1>(int x) { return x; }

template <>
int swapBytes<2>(int x) {
	union { short s; char c[2]; } conv = { (short) x };

	swap(conv.c[0], conv.c[1]);

	return conv.s;
}

template <>
int swapBytes<4>(int x) {
	union { long l; char c[4]; } conv = { x };

	swap(conv.c[0], conv.c[3]);
	swap(conv.c[1], conv.c[2]);

	return conv.l;
}

static bool is_big_endian()
{
    union { long i; char c[4]; } bint = { 0x01020304 };

    return bint.c[0] == 1;
}

enum class Endianness { LittleEndian, BigEndian };

template <Endianness from, Endianness to, class carrier>
carrier recode(carrier x) {
    return from == to? x : swapBytes(x);
}

const Endianness getSystemEndianness() {
    return is_big_endian()
        ? Endianness::BigEndian
        : Endianness::LittleEndian;
}

#endif
