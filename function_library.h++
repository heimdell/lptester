
#ifndef FUNCTION_LIBRARY
#define FUNCTION_LIBRARY

#include <functional>
#include <vector>
#include <map>
#include <stack>
#include <tuple>

#include "dialog.h++"

using namespace std;

/*
    Представление функции положения оси от времени.
 */
struct Mapper
{
    double (*f)(double), kx, ky;

    Mapper(double (*f)(double), double kx, double ky)
      : f (f)
      , kx(kx)
      , ky(ky)
    { }

    double operator() (double x) {
        return ky * f(x * kx);
    }
};

/*
    В данный момент, доступны следущие виды движения:
     - линейное;
     - компоненты кругового.
 */
double id_function (double x) { return x;             }
double sin_function(double x) { return sin(x * M_PI); }
double cos_function(double x) { return cos(x * M_PI); }

#endif