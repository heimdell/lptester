
#ifndef INTERPOLATOR_HPP
#define INTERPOLATOR_HPP

#include <cassert>

#include <functional>

#include "dialog.h++"

/*
    Механизм квантования функции, для вывода её на ось.

    (старая версия)
 */
template <int carrier>
struct Interpolator
{
    double dtime;
    int steps, steps_were;

    double limit    = dtime * carrier / steps;
    double be_error = limit / 2;

    Interpolator(double dtime, int steps)
      : dtime     (dtime)
      , steps     (steps)
      , steps_were(steps)
    { }

    bool done() { return steps <= 0; }

    void reset() { steps = steps_were; }

    bool next() {
        unless (done()) {
            if (++be_error >= limit) {
                be_error -= limit;

                steps--;
                return true;
            }
        }

        return false;
    }
};

/*
    Результат функции на момент времени
 */
enum class Direction {
    FORWARD,  // движение вперёд
    BACKWARD, //          назад
    STAY      // останов
};

/*
    Механизм квантования функции, для вывода её на ось.

    Сформированная функция исполняется заданное число раз на интервале [0..1).
 */
template <int carrier>
struct Function
{
    typedef function<int(double)> mapping;

    double  dtime;
    mapping x;
    int     prev_x;
    int     steps;
    double  max_steps;

    Function(double dtime, mapping x)
      : dtime    (dtime)
      , x        (x)
      , prev_x   (x(0))
      , steps    (0)
      , max_steps(dtime * carrier)
    { }

    bool done() { return steps > max_steps; }

    void reset() { steps = 0; }

    /*
        Получение следущей команды для контроллера оси.
     */
    Direction next() {
        double t = steps++ / max_steps;

        double current_x = x(t);

        // fprintf(stderr, "f(%g) = %g\n", t, current_x);

        // printf("steps = %d, t = %f, prev_x = %d, current_x = %f\n", steps, t, prev_x, current_x);

        /*
            При увеличении функции на единицу - шаг вперёд.
         */
        if (current_x - prev_x >= 1) {
            prev_x++;

            return Direction::FORWARD;
        }

        /*
            При уменьшении - назад.
         */
        if (current_x - prev_x <= -1) {
            prev_x--;

            return Direction::BACKWARD;
        }

        return Direction::STAY;
    }
};

#endif
