
#ifndef LPT_HPP
#define LPT_HPP

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

struct Port {
    fstream    mountPoint;

    Port(string filename)
      : mountPoint (filename)
    { }

    void put (char command) {
        mountPoint << command;
    }

    char get() {
        char dest; mountPoint >> dest;

        return dest;
    }

    ~Port() {
        mountPoint.close();
    }
};

#endif
