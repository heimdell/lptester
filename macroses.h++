
#ifndef MACROSES
#define MACROSES

#define VALUE(x) enum { value = (x) }

#define TYPE(t)  typedef t type

#define ASSIGN(name, value) enum { name = value }

#define self (*this)

#define loop for (;;)

#define s(x) ((string) x)

#define until(preq) while(!(preq))
#define unless(preq) if(!(preq))

#endif
