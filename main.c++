
#include "lpt.h++"

#include "dialog.h++"

unsigned char fromHex(string str) {
    if (str.size() < 2)
        return 0;

    auto h2c = [](char c) {
        return
             c >= '0' && c <= '9' ? c - '0'
           : c >= 'a' && c <= 'f' ? c - 'a' + 10
           : c >= 'A' && c <= 'F' ? c - 'A' + 10
           : 0;
    };

    return h2c(str[0]) * 16 + h2c(str[1]);
}

int main(int argc, char * argv[]) {
    if (argc != 2) {
        cout << "USAGE: lptester <lpt-port-file>" << endl;
        return 1;
    }

    string input;

    Port port(argv[1]);

    loop {
        (cout << "> ").flush();
        getline(cin, input);

        auto tokens  = words(input);
        auto command = head(tokens);

        if (in(command, { s("quit"), s("exit"), s("q"), s(":q"), s("die") } ))
            break;

        if (in(command, { s("put"), s("write") })) {
            if (tokens.size() == 2) {
                port.put(fromHex(tokens[1]));
            } else {
                cout << "! This command must have exactly 1 arg: hexcode to write.";
            }
        }

        if (command == "send") {
            for (auto i = tokens.begin() + 1; i < tokens.end(); i++) {
                auto buf = *i + ' ';

                for (auto j = buf.begin(); j < buf.end(); j++) {
                    port.put(*j);
                }
            }
        }

        cout << endl;
    }

    cout << "# Done." << endl;
}
