
#ifndef MOVE_HPP
#define MOVE_HPP

#include <cmath>

#include <tuple>

#include "interpolator.h++"

#include "bits.h++"

#include "function_library.h++"

/*
    Шаблон настроек оси.
 */
template
  < int FWD_
  , int BKWD_
  >
struct Ax
{
    enum
      { FWD  = bit(FWD_)
      , BKWD = bit(BKWD_)
      };
};

/*
    Шаблон настроек 3х-осевого станка.
 */
template
  < class X_
  , class Y_
  , class Z_
  , int Null_ = 0
  >

struct Pins {
    typedef X_ X;
    typedef Y_ Y;
    typedef Z_ Z;

    enum { Null = Null_ };
};

/*
    Представление единичной операции на станке (шага).
 */
template <int CARRIER, class Pins>
struct Move
{
    /*
        Функции, управляющие смещением осей.
     */
    vector<Function<CARRIER>> axises;

    /*
        Маски для каждой оси.
     */
    vector<tuple<char, char>> masks;

    /*
        Предопределённый пустой шаг.
     */
    static Move<CARRIER, Pins> StandStill;

    /*
        Шаг определяется временем проведения и поведением осей на этом шаге.
     */
    Move(double dtime, vector<Mapper> xs)
    {
        // настройка функций осей
        for (auto x : xs) {
            axises.emplace_back(dtime, x);

            masks.push_back(make_tuple(0, 0));
        }

        #define put_safe(i,axis)       \
            if (masks.size() > (i))    \
                masks[i] = make_tuple  \
                    ( Pins::axis::FWD  \
                    , Pins::axis::BKWD \
                    )

        / установка масок
        put_safe(0, X);
        put_safe(1, Y);
        put_safe(2, Z);
    }

    /*
        Получение следущего байта-команды для станка.
     */
    char next() {
        /*
            Отработанная команда возвразщает пустую команду (защита от переполнения по времени).
         */
        if (done()) {
            return Pins::Null;
        }
        else {
            char accum = 0;

            for (unsigned i = 0; i < axises.size(); i++) {
                /*
                    Выставляем маски для всех осей.
                 */
                switch (axises[i].next()) {
                case Direction::FORWARD:
                    accum |= get<0>(masks[i]);
                    break;

                case Direction::BACKWARD:
                    accum |= get<1>(masks[i]);

                default:;
                }
            }

            /* 
                В случае отсутствия движения, возвращаем пустую команду.
            */
            return accum ? accum : (char) Pins::Null;
        }
    }

    /*
        Шаг завершён, когда отработали все оси.

        Защита от "дребезжания" интерполяции.
     */
    bool done() {
        for (auto axis : axises) {
            unless (axis.done())
                return false;
        }

        return true;
    }

    /*
        Перезапуск управляющих функций осей.
     */
    void reset() {
        for (auto axis : axises)
            axis.reset();
    }
};

template <int CARRIER, class Pins>
Move<CARRIER, Pins> Move<CARRIER, Pins>::StandStill = { 0, {} };


#endif
