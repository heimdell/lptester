
#ifndef PROGRAM_READER_HPP
#define PROGRAM_READER_HPP

#include "move.h++"

#include "dialog.h++"

template <class VM>
struct Checher
{
    void line(string filename, int line, double dtime, vector<int> xs) {
        int carrier   = VM::CARRIER;
        int max_steps = dtime * carrier;

        auto axis_name = [](unsigned n) {
            static vector<string> names = { "X", "Y", "Z", "U", "V", "W" };

            return n < names.size() 
                ? names[n] 
                : "(unnamed)";
        };

        unsigned i = 0;

        for (auto steps : xs) 
        {
            if (max_steps < abs(steps)) 
            {
                throw runtime_error(
                    unlines
                      ( "Error in program.", ""
                      , unlines
                          ( "Error type: "
                          , "  STEP_COUNT_GREATER_THAN_POSSIBLE"
                          )
                      , unlines
                          ( "Location: "
                          , glue("  filename  = ", filename)
                          , glue("  line      = ", line)
                          , glue("  axis      = ", i)
                          , glue("  axis name = ", axis_name(i))
                          )
                      , unlines
                          ( "Values: "
                          , glue("  dtime     = ", dtime)
                          , glue("  carrier   = ", carrier)
                          , glue("  max steps = ", max_steps)
                          , glue("  steps     = ", steps)
                          )
                      , unlines
                          ( "Description:"
                          , glue
                              ( "  Count of steps requested (", abs(steps), ")"
                              , " exceedes the count of steps the controller is able to perform (", max_steps, ")"
                              , " in given time (", dtime, " sec)"
                              , "."
                              )
                          , ""
                          , "  Max step count is calculated as carrier * dtime."
                          , ""
                          )
                      )
                );
            }

            i++;
        }
    }
};

/*
    Механизм чтения программы.
 */
template <class VM>
struct ProgramReader
{
    typedef vector<typename VM::Command> Program;

    Program readFrom(string filename) {

        Program program;

        string file;

        if (FILE *fp = fopen(filename.c_str(), "r"))
        {
            char buf[1024];

            while (size_t len = fread(buf, 1, sizeof(buf), fp))
                file.insert(file.end(), buf, buf + len);

            fclose(fp);
        }

        /*
            Разбиваем входной файл на строки.

            Пример формата строки - "10, cos 2 20000, sin 2 20000"
         */
        vector<string> lines = words(file, isLn);

        int line_num = -1;

        vector<Mapper> xs;

        for (auto line : lines) {
            line_num++;

            /*
                Считываем время выполнения команды.
             */
            auto axises = words(line, [](char c) { return c == ','; });

            auto dtime = read<double>(axises.front());

            tail(axises);

            vector<Mapper> xs;

            int axis_num = -1;

            /*
                Считываем поведение осей во время выполненя команды.
             */
            for (auto axis : axises) {
                axis_num++;

                auto tokens = words(axis);

                /*
                    Вычитываем имя функции.
                 */
                auto name = tokens.front();

                /*
                    Отделяем коэффициенты при x и y.
                 */
                tail(tokens);

                unless (tokens.size() != 1) {
                    throw (runtime_error(
                        glue(
                            "at the line", line_num, ", "
                            "at the axis", axis_num, ", "
                            "expected 2 params, "
                            "but found", tokens.size()
                        )
                    ));
                }

                auto x = read<double>(tokens[0]);
                auto y = read<double>(tokens[1]);

                /*
                    Создаём маппер.
                 */
                auto fun = Mapper(
                    name == "line" ? id_function  :
                    name == "sin"  ? sin_function :
                                     cos_function,
                    x, y);

                /*
                    Добавляем его в рписание шага.
                 */
                xs.push_back(fun);
            }

            /*
                Добавляем шаг в программу.
             */
            program.emplace_back(dtime, xs);
        }

        return program;
    }

};

#endif
