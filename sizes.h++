
#ifndef SIZES_HPP
#define SIZES_HPP

#include "macroses.h++"

template <int> struct typeOfSize;

template <> struct typeOfSize<1> { TYPE(char);  };
template <> struct typeOfSize<2> { TYPE(short); };
template <> struct typeOfSize<4> { TYPE(int);   };

#endif
